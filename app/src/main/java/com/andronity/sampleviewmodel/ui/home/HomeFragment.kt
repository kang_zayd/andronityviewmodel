package com.andronity.sampleviewmodel.ui.home

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.andronity.sampleviewmodel.R
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var order = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        text_home.text = "${homeViewModel.order}"
//        text_home.text = "$order"

        btnAdd.setOnClickListener {
            homeViewModel.order++
            text_home.text = "${homeViewModel.order}"/*
            order++
            text_home.text = "$order"*/
        }
        val pd= ProgressDialog(activity)
        pd.setMessage("Get data...")
        /*
        pilih salah satu method
         */
        homeViewModel.getData() // apicall from viewmodel
//        homeViewModel.getDataUsingRepo() // apicall from repo
        /*
        =============
         */
        homeViewModel.isLoading.observe(this, Observer { if (it)pd.show() else pd.dismiss() })
        homeViewModel.message.observe(this, Observer {
            it?.let { msg-> Toast.makeText(activity, msg, Toast.LENGTH_LONG).show() }
        })
        homeViewModel.data.observe(this, Observer {
            // code set data to adapter recyclerview

        })
    }
}