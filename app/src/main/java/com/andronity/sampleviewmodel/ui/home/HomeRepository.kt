package com.andronity.sampleviewmodel.ui.home

import com.andronity.sampleviewmodel.ApiCall
import com.andronity.sampleviewmodel.SemuaprovinsiItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/*
 * Custom Repo
 * Create by Muhamad Sa'id Abdurrahman
 */

fun callGetData(
    onRespone: (isLoading: Boolean, message: String?, List<SemuaprovinsiItem>?) -> Unit) =
    ApiCall.instance().getData()
        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        .subscribe({
            if (it.isSuccessful) {
                onRespone(false, it.body()?.message, it.body()?.semuaprovinsi)
            } else {
                onRespone(false, it.message(), null)
            }
        }, {
            onRespone(false, it.message, null)
        })
