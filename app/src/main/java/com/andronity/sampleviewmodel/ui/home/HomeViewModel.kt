package com.andronity.sampleviewmodel.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.andronity.sampleviewmodel.ApiCall
import com.andronity.sampleviewmodel.SemuaprovinsiItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/*
 * Create by Muhamad Sa'id Abdurrahman
 */

class HomeViewModel : ViewModel() {

    var order = 0
    private val compositeDisposable = CompositeDisposable()
    var isLoading = MutableLiveData<Boolean>()
    var message = MutableLiveData<String>()
    var data = MutableLiveData<List<SemuaprovinsiItem>>()

    fun getData() {
        isLoading.value = true
        compositeDisposable.add(
            ApiCall.instance().getData()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isLoading.value = false
                    if (it.isSuccessful) {
                        message.value = it.body()?.message
                        it.body()?.semuaprovinsi?.let { list -> data.value = list }
                    } else {
                        message.value = it.message()
                    }
                }, {
                    isLoading.value = false
                    message.value = "${it.message}"
                })
        )
    }

    fun getDataUsingRepo(){
        isLoading.value = true
        compositeDisposable.add(
            callGetData { isLoading, message, list ->
                this.isLoading.value = isLoading
                this.message.value = message
                list?.let { datas -> this.data.value = datas }
            }
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}