package com.andronity.sampleviewmodel

import com.google.gson.annotations.SerializedName

data class ResponseProvinsi(

	@field:SerializedName("error")
	val error: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("semuaprovinsi")
	val semuaprovinsi: List<SemuaprovinsiItem>? = null
)

data class SemuaprovinsiItem(

	@field:SerializedName("nama")
	val nama: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)